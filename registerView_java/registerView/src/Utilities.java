//------------------------------------------------------------------------------
// Copyright (c) 2016-2019, Michel RIZZO (the 'author' in the following)
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------
import org.apache.commons.lang3.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

class Utilities {
	//--------------------------------------------------------------------------
	static String long2HexString(long l, int i) {
		return StringUtils.leftPad(Long.toHexString(l), i, '0').toUpperCase();
	}
	//--------------------------------------------------------------------------
	static String long2BinString(long l, int i) {
		return "0b" + StringUtils.leftPad(Long.toBinaryString(l), i, '0');
	}
	static String int2BinString(int i, int n) {
		return "0b" + StringUtils.leftPad(Integer.toBinaryString(i), n, '0');
	}
	//--------------------------------------------------------------------------
	static long computeMask(int width, int startingbit) {
		long mask = 0;
		for (int ¢ = 0; ¢ < width; ++¢)
			mask = (mask << 1) | 1;
		return mask <<= startingbit;
	}
	//--------------------------------------------------------------------------
	static void ErrorBox(Shell shell, String Message) {
		MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_ERROR);
		messageBox.setMessage(Message);
		messageBox.open();
	}
}
//==============================================================================
