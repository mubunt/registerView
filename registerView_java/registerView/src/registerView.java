//------------------------------------------------------------------------------
// Copyright (c) 2016-2019, Michel RIZZO (the 'author' in the following)
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------
import org.apache.commons.cli.*;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class registerView {
	private static final String sTitle = "Configuration Registers Viewer";
	private static final String xmlConfiguration = "configuration";
	private static final String xmlConfigurationTitle = "title";
	private static final String xmlRegister = "register";
	private static final String xmlRegisterId = "id";
	private static final String xmlFieldName = "name";
	private static final String xmlFieldLabel = "label";
	private static final String xmlFieldWidth = "width";
	private static final String xmlFieldBit = "bit";
	private static final String xmlValueLabel = "label";
	private static final String xmlValueVal = "val";
	private static final String keywordRESERVED = "RESERVED";
	private static int intUnic;
	private static boolean debugMode;

	enum ValueSelection {
		NOTSELECTED, SELECTED
	}

	enum ValueCorrectness {
		CORRECT, WRONG, ACCEPTABLE
	}

	static class valueDefinition {
		String unic;
		int value;
		String label;
		ValueSelection status;

		int getValue() { return this.value; }
		String getLabel() { return this.label; }
		ValueSelection getStatus() { return this.status; }

		void setUnic() { this.unic = String.valueOf(intUnic); ++intUnic; }
		void setValue(int value) { this.value = value; }
		void setLabel(String label) { this.label = label; }
		void setStatus(ValueSelection status) { this.status = status; }
	}

	static class fieldDefinition {
		String name;
		String label;
		int width;
		long mask;
		int bit;
		long value;
		valueDefinition[] values;
		ValueCorrectness status;

		String getName() { return this.name; }
		String getLabel() { return this.label; }
		int getWidth() { return this.width; }
		long getMask() { return this.mask; }
		int getBit() { return this.bit; }
		long getValue() { return this.value; }
		ValueCorrectness getStatus() { return this.status; }

		void setName(String name) { this.name = name; }
		void setLabel(String label) { this.label = label; }
		void setWidth(int width) { this.width = width; }
		void setMask(long mask) { this.mask = mask; }
		void setBit(int bit) { this.bit = bit; }
		void setValue(long value) { this.value = value; }
		void setStatus(ValueCorrectness status) { this.status = status; }
	}

	static class registerDefinition {
		String name;
		long value;
		fieldDefinition[] fields;
		ValueCorrectness status;
		Text input;

		String getName() { return this.name; }
		long getValue() { return this.value; }
		ValueCorrectness getStatus() { return this.status; }
		Text getInput() { return this.input; }

		void setName(String name) { this.name = name; }
		void setValue(long value) { this.value = value; }
		void setStatus(ValueCorrectness status) { this.status = status; }
		void setInput(Text input) { this.input = input; }
	}

	static registerDefinition[] Registers;
	static String Title = "Register View";
	//==========================================================================
	public static void main(String[] args) {
		String xmlConfigFile = null;
		//----------------------------------------------------------------------
		// OPTIONS
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("h", "help",       false, "print this message and exit");
		options.addOption("V", "version",    false, "print the version information and exit");
		options.addOption("x", "xml",        true,  "XML file description");
		options.addOption("d", "debug",      false, "debug mode");
		try {
			CommandLine line = parser.parse(options, args);
			// Option: Help
			if (line.hasOption("h")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(sTitle, options);
				System.exit(0);
			}
			// Option: Version
			if (line.hasOption("V")) {
				System.out.println(sTitle +
				 	"	Version: " + registerViewBuildInfo.getVersion() +
					" - Build: " + registerViewBuildInfo.getNumber() +
					" - Date: " + registerViewBuildInfo.getDate());
				System.exit(0);
			}
			// Option: Xml
			if (line.hasOption("x"))
				xmlConfigFile = line.getOptionValue("x");
			// Option: Debug
			if (line.hasOption("d"))
				debugMode = true;
		} catch (ParseException exp) {
			System.err.println("!!! ERROR !!! " + sTitle + ": " + "Option parsing failed - " + exp.getMessage());
			System.exit(-1);
		}

		if (debugMode) {
			String myLocation = registerView.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			System.out.println("myLocation=<" + myLocation + ">");
			System.out.println("xmlConfigFile=<" + xmlConfigFile + ">");
		}

		readConfiguration(xmlConfigFile);
		decodeConfiguration();
		viewConfiguration();

		System.exit(0);
	}
	//--------------------------------------------------------------------------
	private static void readConfiguration(String xmlConfigFile) {
		Document doc = null;

		try {
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse((new File(xmlConfigFile)));
		} catch (Exception e) {
			System.err.println("!!! ERROR !!! " + sTitle + ": " + "XML parsing failed - " + e.getMessage());
			System.exit(-1);
		}
		doc.getDocumentElement().normalize();

		NodeList titleList = doc.getElementsByTagName(xmlConfiguration);
		if (titleList.getLength() > 0)
			Title = ((Element) titleList.item(0)).getAttribute(xmlConfigurationTitle);

		NodeList regList = doc.getElementsByTagName(xmlRegister);
		Registers = new registerDefinition[regList.getLength()];

		for (int i = 0; i < regList.getLength(); ++i) {
			Registers[i] = new registerDefinition();
			Registers[i].setName(null);
			Registers[i].setValue(0);
			Registers[i].setStatus(ValueCorrectness.CORRECT);

			Node registerNode = regList.item(i);
			if (registerNode.getNodeType() == Node.ELEMENT_NODE) {
				Element regElement = (Element) registerNode;
				Registers[i].setName(regElement.getAttribute(xmlRegisterId));
				String s = regElement.getAttribute("default").replaceAll("0x", "");
				try {
					Registers[i].setValue(Long.parseLong(s, 16));
				} catch (NumberFormatException e) {
					System.err.println("!!! ERROR !!! XML parsing failed - Wrong value for "
							+ Registers[i].getName());
					Registers[i].setValue(0);
					Registers[i].setStatus(ValueCorrectness.WRONG);
				}

				NodeList fieldList = registerNode.getChildNodes();
				Registers[i].fields = new fieldDefinition[fieldList.getLength()];

				for (int j = 0; j < fieldList.getLength(); ++j) {
					Registers[i].fields[j] = new fieldDefinition();
					Registers[i].fields[j].setName(null);
					Registers[i].fields[j].setLabel(null);
					Registers[i].fields[j].setWidth(0);
					Registers[i].fields[j].setMask(0);
					Registers[i].fields[j].setBit(0);
					Registers[i].fields[j].setValue(0);
					Registers[i].fields[j].setStatus(ValueCorrectness.CORRECT);

					Node fieldNode = fieldList.item(j);
					if (fieldNode.getNodeType() == Node.ELEMENT_NODE) {
						Element fieElement = (Element) fieldNode;
						Registers[i].fields[j].setName(fieElement.getAttribute(xmlFieldName));
						Registers[i].fields[j].setLabel(fieElement.getAttribute(xmlFieldLabel));
						try {
							Registers[i].fields[j].setWidth(Integer.parseInt(fieElement.getAttribute(xmlFieldWidth)));
						} catch (NumberFormatException e) {
							System.err.println("!!! ERROR !!! XML parsing failed - Wrong width for "
									+ Registers[i].getName()
									+ " / "
									+ Registers[i].fields[j].getName());
							Registers[i].fields[j].setWidth(0);
							Registers[i].setStatus(ValueCorrectness.WRONG);
							Registers[i].fields[j].setStatus(ValueCorrectness.WRONG);
						}

						try {
							Registers[i].fields[j].setBit(Integer.parseInt(fieElement.getAttribute(xmlFieldBit)));
						} catch (NumberFormatException e) {
							System.err.println("!!! ERROR !!! XML parsing failed - Wrong bit number for "
									+ Registers[i].getName()
									+ " / "
									+ Registers[i].fields[j].getName());
							Registers[i].fields[j].setBit(0);
							Registers[i].setStatus(ValueCorrectness.WRONG);
							Registers[i].fields[j].setStatus(ValueCorrectness.WRONG);
						}
						Registers[i].fields[j].setMask(Utilities.computeMask(Registers[i].fields[j].getWidth(), Registers[i].fields[j].getBit()));

						NodeList valueList = fieldNode.getChildNodes();
						Registers[i].fields[j].values = new valueDefinition[valueList.getLength()];

						for (int k = 0; k < valueList.getLength(); ++k) {
							Registers[i].fields[j].values[k] = new valueDefinition();
							Registers[i].fields[j].values[k].setUnic();
							Registers[i].fields[j].values[k].setLabel(null);
							Registers[i].fields[j].values[k].setValue(0);
							Registers[i].fields[j].values[k].setStatus(ValueSelection.NOTSELECTED);

							Node valueNode = valueList.item(k);
							if (valueNode.getNodeType() == Node.ELEMENT_NODE) {
								Element valElement = (Element) valueNode;
								Registers[i].fields[j].values[k].setLabel(valElement.getAttribute(xmlValueLabel));
								try {
									Registers[i].fields[j].values[k].setValue(Integer.parseInt(valElement.getAttribute(xmlValueVal)));
								} catch (NumberFormatException e) {
									System.err.println("!!! ERROR !!! XML parsing failed - Wrong value for "
											+ Registers[i].getName()
											+ " / "
											+ Registers[i].fields[j].getName()
											+ " / "
											+ Registers[i].fields[j].values[k].getLabel());
									Registers[i].fields[j].values[k].setValue(0);
									Registers[i].setStatus(ValueCorrectness.WRONG);
									Registers[i].fields[j].setStatus(ValueCorrectness.WRONG);
								}
							}
						}
					}
				}
			}
		}
	}
	//--------------------------------------------------------------------------
	private static void viewConfiguration() {
		View.viewConfiguration();
	}
	//--------------------------------------------------------------------------
	static void decodeConfiguration() {
		for (registerDefinition Register : Registers)
			if (Register.getName() != null) {
				Register.setStatus(ValueCorrectness.CORRECT);
				for (int j = 0; j < Register.fields.length; ++j)
					if (Register.fields[j].getName() != null) {
						long tmp = (Register.getValue()
								& Register.fields[j].getMask()) >> Register.fields[j].getBit();
						if (Register.fields[j].getName().equals(keywordRESERVED))
							if (tmp == 0)
								Register.fields[j].setStatus(ValueCorrectness.CORRECT);
							else {
								Register.fields[j].setStatus(ValueCorrectness.ACCEPTABLE);
								if (Register.getStatus() == ValueCorrectness.CORRECT)
									Register.setStatus(ValueCorrectness.ACCEPTABLE);
								Register.fields[j].setValue(tmp);
							}
						else {
							if (Register.fields[j].values.length != 0) {
								Register.fields[j].setStatus(ValueCorrectness.WRONG);
								boolean found = false;
								for (int k = 0; k < Register.fields[j].values.length; ++k)
									if (Register.fields[j].values[k].getLabel() != null) {
										if (tmp != Register.fields[j].values[k].getValue())
											Register.fields[j].values[k].setStatus(ValueSelection.NOTSELECTED);
										else {
											Register.fields[j].values[k].setStatus(ValueSelection.SELECTED);
											Register.fields[j].setStatus(ValueCorrectness.CORRECT);
											Register.fields[j].setValue(tmp);
											found = true;
										}
									}
								if (!found)
									Register.setStatus(ValueCorrectness.WRONG);
							}
							Register.fields[j].setValue(tmp);
						}
					}
			}
		if (debugMode) listConfiguration();
	}
	//--------------------------------------------------------------------------
	static void encodeConfiguration(int index) {
		long value = 0;
		for (int j = 0; j < Registers[index].fields.length; ++j)
			if (Registers[index].fields[j].getName() != null)
				value |= Registers[index].fields[j].getValue() << Registers[index].fields[j].getBit();
		Registers[index].setValue(value);
	}
	//--------------------------------------------------------------------------
	private static void listConfiguration() {
		for (registerDefinition Register : Registers) {
			if (Register.getName() != null) {
				System.out.println("Register: " + Register.getName() + " - Value: 0x"
						+ Utilities.long2HexString(Register.getValue(), 8) + " - Status: "
						+ Register.getStatus());
				for (int j = 0; j < Register.fields.length; ++j) {
					if (Register.fields[j].getName() != null) {
						System.out.println("\tField: " + Register.fields[j].getName() + " - Label: "
								+ Register.fields[j].getLabel() + " - Width: " + Register.fields[j].getWidth()
								+ " - Mask: 0x" + Utilities.long2HexString(Register.fields[j].getMask(), 8)
								+ " - Bit: " + Register.fields[j].getBit() + " - Value: "
								+ Utilities.long2BinString(Register.fields[j].getValue(),
								Register.fields[j].getWidth())
								+ " - Status: " + Register.fields[j].getStatus());
						for (int k = 0; k < Register.fields[j].values.length; ++k)
							if (Register.fields[j].values[k].getLabel() != null) {
								System.out.println("\t\tValue: "
										+ Utilities.int2BinString(Register.fields[j].values[k].getValue(),
										Register.fields[j].getWidth())
										+ " - Label: " + Register.fields[j].values[k].getLabel() + " - Status: "
										+ Register.fields[j].values[k].getStatus());
							}
					}
				}
			}
		}
	}
}
//==============================================================================
