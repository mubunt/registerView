//------------------------------------------------------------------------------
// Copyright (c) 2016-2019, Michel RIZZO (the 'author' in the following)
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabFolder2Adapter;
import org.eclipse.swt.custom.CTabFolderEvent;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

class View {
	private static Shell shell;
	private static Color ColorActive 	= new Color(Display.getDefault(), 255, 215, 0); // Yellow gold
	private static Color ColorError		= new Color(Display.getDefault(), 255,  0, 0);	// Red
	private static Color ColorWarning	= new Color(Display.getDefault(), 255,  140, 0);// DarkOrange

	private static Font fontHeader1		= new Font(Display.getCurrent(), "Arial", 10, SWT.BOLD);
	private static Font fontNormal		= new Font(Display.getCurrent(), "Arial", 10, SWT.NORMAL);
	private static Font fontBold		= new Font(Display.getCurrent(), "Arial", 10, SWT.BOLD);
	private static Font fontItalic		= new Font(Display.getCurrent(), "Arial", 10, SWT.ITALIC);
	private static Font fontGroup		= new Font(Display.getCurrent(), "Arial", 10, SWT.BOLD);

	private static final int widthTreeColumn1	= 120;
	private static final int widthTreeColumn2	= 180;
	private static final int widthTreeColumn3	= 300;
	//--------------------------------------------------------------------------
	static void viewConfiguration() {
		shell = new Shell(Display.getCurrent(), SWT.MAX | SWT.MIN | SWT.CLOSE | SWT.TITLE | SWT.BORDER | SWT.RESIZE);
		shell.setText(registerView.Title);
		shell.setLayout(new GridLayout());
		shell.setCursor(Display.getCurrent().getSystemCursor(SWT.CURSOR_ARROW));
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		shell.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		CTabFolder tabFolder = new CTabFolder(shell, SWT.NONE);
		tabFolder.setBorderVisible(false);
		tabFolder.setSimple(false);
		tabFolder.setLayout(new GridLayout());
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 0, 0));
		tabFolder.setSelectionBackground(ColorActive);
		tabFolder.addCTabFolder2Listener(new CTabFolder2Adapter() {
			public void close(CTabFolderEvent __) { }
		});

		for (int i = 0; i < registerView.Registers.length; ++i) {
			final int index = i;
			if (registerView.Registers[i].getName() != null ) {
				CTabItem tabView = new CTabItem (tabFolder, SWT.NONE);
				tabView.setText("     " + registerView.Registers[i].getName() + " Register     ");
				tabView.setFont(fontNormal);
				tabView.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
				tabView.setSelectionForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
				Composite composite = new Composite(tabFolder, SWT.NONE);
				tabView.setControl(composite);
				composite.setLayout(new GridLayout());

				Composite A = new Composite(composite, SWT.NONE);
				A.setLayout(new GridLayout(2, false));
				//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
				Group group0 = new Group (A, SWT.NONE);
				group0.setFont(fontGroup);
				group0.setText(" Register ");
				group0.setLayout(new GridLayout(3, false));
				group0.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, true, 0, 0));
				Label Value = new Label (group0, SWT.NONE);
				Value.setFont(fontHeader1);
				Value.setText("Value: 0x");

				registerView.Registers[i].setInput(new Text(group0, SWT.BORDER));
				registerView.Registers[i].getInput().setTextLimit(8);
				registerView.Registers[i].getInput().forceFocus();
				registerView.Registers[i].getInput().setText(Utilities.long2HexString(registerView.Registers[i].getValue(), 8));
				registerView.Registers[i].getInput().setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
				registerView.Registers[i].getInput().setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
				Button ok = createButton(group0, "   DECODE   ");
				//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
				Group group1 = new Group (A, SWT.NONE);
				group1.setFont(fontGroup);
				group1.setText(" Fields ");
				group1.setLayout(new GridLayout(2, false));
				group1.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, true, 0, 0));
				Button expand = createButton(group1, " Expand ");
				//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
				final Tree tree = new Tree(composite, SWT.FULL_SELECTION);
				tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 0, 0));
				tree.setHeaderVisible(false);
				tree.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
				tree.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
				TreeColumn column1 = new TreeColumn(tree, SWT.LEFT);
				column1.setWidth(widthTreeColumn1);
				TreeColumn column2 = new TreeColumn(tree, SWT.LEFT);
				column2.setWidth(widthTreeColumn2);
				TreeColumn column3 = new TreeColumn(tree, SWT.LEFT);
				column3.setWidth(widthTreeColumn3);

				registerView.Registers[i].getInput().addKeyListener(new KeyAdapter() {
					public void keyPressed(KeyEvent e) {
						if (e.keyCode != SWT.CR)
							return;
						try {
							registerView.Registers[index]
									.setValue(Long.parseLong(registerView.Registers[index].getInput().getText(), 16));
						} catch (NumberFormatException f) {
							Utilities.ErrorBox(shell,
									"Wrong hexadecimal number: " + registerView.Registers[index].getInput().getText());
						}
						registerView.decodeConfiguration();
						DisplayTree(tree, index);
					}
				});

				ok.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent __) {
						try {
							registerView.Registers[index].setValue(Long.parseLong(registerView.Registers[index].getInput().getText(), 16));
						} catch (NumberFormatException f) {
							Utilities.ErrorBox(shell, "Wrong hexadecimal number: " + registerView.Registers[index].getInput().getText());
						}
						registerView.decodeConfiguration();
						DisplayTree(tree, index);
					}
				});

				expand.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent __) {
						expandTreeItems(tree, true);
					}
				});
				Button collapse = createButton(group1, " Collapse ");
				collapse.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent __) {
						expandTreeItems(tree, false);
					}
				});
				DisplayTree(tree, index);
			}
		}
		tabFolder.pack();
		shell.pack();
		shell.setSize (700, 550);
		shell.open();
		while (!shell.isDisposed())
			if (!Display.getCurrent().readAndDispatch())
				Display.getCurrent().sleep();
		fontHeader1.dispose();
		fontNormal.dispose();
		fontBold.dispose();
		fontItalic.dispose();
		fontGroup.dispose();
		Display.getCurrent().dispose();
	}
	//--------------------------------------------------------------------------
	private static Button createButton(Group g, String label) {
		Button b = new Button(g, SWT.PUSH);
		b.setText(label);
		b.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		b.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		return b;
	}
	//--------------------------------------------------------------------------
	private static void DisplayTree(final Tree t, final int idx) {
		String s;
		t.removeAll();
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		TreeItem iItem = new TreeItem(t, SWT.NONE);
		s = "";
		if (registerView.Registers[idx].getStatus() == registerView.ValueCorrectness.WRONG) {
			s = "WRONG VALUE";
			iItem.setForeground(ColorError);
		}
		if (registerView.Registers[idx].getStatus() == registerView.ValueCorrectness.ACCEPTABLE) {
			s = "Acceptable value but may be refined";
			iItem.setForeground(ColorWarning);
		}
		iItem.setFont(fontBold);
		iItem.setText(new String[] {
				registerView.Registers[idx].getName(),
				"0x" + Utilities.long2HexString(registerView.Registers[idx].getValue(), 8),
				s});
		iItem.setExpanded(true);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		for (int j = 0; j < registerView.Registers[idx].fields.length; ++j)
			if (registerView.Registers[idx].fields[j].getName() != null) {
				TreeItem jItem = new TreeItem(iItem, SWT.NONE);
				s = registerView.Registers[idx].fields[j].getLabel();
				if (registerView.Registers[idx].fields[j].getStatus() == registerView.ValueCorrectness.WRONG) {
					s += " - WRONG VALUE";
					jItem.setForeground(ColorError);
					jItem.setFont(fontBold);
				}
				if (registerView.Registers[idx].fields[j].getStatus() != registerView.ValueCorrectness.ACCEPTABLE)
					jItem.setFont(fontNormal);
				else {
					s += " - Acceptable value but should 0x0";
					jItem.setForeground(ColorWarning);
					jItem.setFont(fontBold);
				}
				jItem.setText(new String[] { registerView.Registers[idx].fields[j].getName(),
						Utilities.long2BinString(registerView.Registers[idx].fields[j].getValue(),
								registerView.Registers[idx].fields[j].getWidth()),
						s });
				jItem.setExpanded(true);
				for (int k = 0; k < registerView.Registers[idx].fields[j].values.length; ++k)
					if (registerView.Registers[idx].fields[j].values[k].getLabel() != null) {
						TreeItem kItem = new TreeItem(jItem, SWT.NONE);
						kItem.setFont(registerView.Registers[idx].fields[j].values[k]
								.getStatus() == registerView.ValueSelection.SELECTED ? fontBold : fontItalic);
						kItem.setText(new String[] { "",
								Utilities.int2BinString(registerView.Registers[idx].fields[j].values[k].getValue(),
										registerView.Registers[idx].fields[j].getWidth()),
								registerView.Registers[idx].fields[j].values[k].getLabel() });
					}
			}
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		t.addListener(SWT.MouseDoubleClick, __ -> {
            int ifield = 0;
            TreeItem[] selection = t.getSelection();
            if (selection.length == 0 || selection[0].getParentItem() == null
                    || selection[0].getParentItem().getParentItem() == null)
                return;
            String s1 = selection[0].getParentItem().getText(0);
            for (int v = Integer.parseInt(selection[0].getText(1).replaceAll("0b", ""),
                    2), j = 0; j < registerView.Registers[idx].fields.length; ++j)
                if (registerView.Registers[idx].fields[j].getName() != null
                        && registerView.Registers[idx].fields[j].getName().equals(s1)) {
                    ifield = j;
                    for (int k = 0; k < registerView.Registers[idx].fields[j].values.length; ++k)
                        if (registerView.Registers[idx].fields[j].values[k].getLabel() != null
                                && registerView.Registers[idx].fields[j].values[k]
                                        .getStatus() == registerView.ValueSelection.SELECTED) {
                            registerView.Registers[idx].fields[j].values[k]
                                    .setStatus(registerView.ValueSelection.NOTSELECTED);
                            break;
                        }
                    for (int k = 0; k < registerView.Registers[idx].fields[j].values.length; ++k)
                        if (registerView.Registers[idx].fields[j].values[k].getLabel() != null
                                && registerView.Registers[idx].fields[j].values[k].getValue() == v) {
                            registerView.Registers[idx].fields[j].values[k]
                                    .setStatus(registerView.ValueSelection.SELECTED);
                            break;
                        }
                    registerView.Registers[idx].fields[j].setValue(v);
                    break;
                }
            registerView.encodeConfiguration(idx);
            registerView.Registers[idx].getInput()
                    .setText(Utilities.long2HexString(registerView.Registers[idx].getValue(), 8));
            DisplayTree(t, idx);
            expandTreeItem(t, registerView.Registers[idx].fields[ifield].getName());
        });
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		t.getItems()[0].setExpanded(true);
		t.layout();
	}
	//--------------------------------------------------------------------------
	private static void expandTreeItems(Tree t, boolean expand) {
		TreeItem[] treeItems = t.getItems();
		if (treeItems != null)
			for (TreeItem treeItem : treeItems) {
				treeItem.setExpanded(true);
				TreeItem[] treeItems2 = treeItem.getItems();
				if (treeItems2 != null)
					for (TreeItem treeItem2 : treeItems2)
						treeItem2.setExpanded(expand);
			}
	}
	//--------------------------------------------------------------------------
	private static void expandTreeItem(Tree t, String field) {
		TreeItem[] treeItems = t.getItems();
		if (treeItems != null)
			for (TreeItem treeItem : treeItems) {
				String s1 = (treeItem + "").replace("TreeItem {", "").replace("}", "");
				if (s1.equals(field)) {
					treeItem.setExpanded(true);
					break;
				}
				TreeItem[] treeItems2 = treeItem.getItems();
				if (treeItems2 != null)
					for (TreeItem treeItem2 : treeItems2)
						if ((treeItem2 + "").replace("TreeItem {", "").replace("}", "").equals(field)) {
							treeItem2.setExpanded(true);
							break;
						}
			}
	}
}
//==============================================================================
