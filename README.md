# APPLICATION: *registerView*

A generic viewer to display and determine the value of  registers described as XML.

***

In this document, and in the application it describes, a register is a hardware or software user-configurable 32-bit register that determines which specific system parameters are set during initialization. The configuration of each parameter is a set of bits named field in the remainder of this document.

The **Registers Viewer** (**registerView**) is a host Java application, designed over SWT and Apache Commons libraries,  and running on Linux and Windows. SWT, Standard Widget Toolkit, is an open source widget toolkit for Java designed by Eclipse Consortium to provide efficient, portable access to the user-interface facilities of the operating systems on which it is implemented. The Apache Commons is a project of the Apache Software Foundation;  the purpose of the Commons is to provide reusable, open source Java software.

### LICENSE
**registerView** is covered by the GNU General Public License (GPL) version 3 and above.

### XML DESCRIPTION OF REGISTERS
This section describes the XML format used to represent a set of registers.

	<?xml version="1.0"?>
	<configuration title="NameOfRegisterSet"\>
		<register id="RegisterName" default="Hex32bitValue">
			<field name="FieldName" label="ShortFieldDescription" width="NumberOfBits" bit="StartingBit">
				<value val="FieldValue" label="ShortValueDescription"/>
				<value val="FieldValue" label="ShortValueDescription"/>
				...
			</field>
			<field name="FieldName" label="ShortFieldDescription" width="NumberOfBits" bit="StartingBit">
			...
			</field>
			...
		</register>
		<register id="RegisterName" default="Hex32bitValue">
		...
		</register>
		...
	</configuration>
	
#### Register set description
The root element required for a set of registers description1 is named configuration. It has one attribute, title, that provides the name of the the register set.

Example:

	<?xml version="1.0"?>
	<configuration title="Chartreuse71 Registers">
	…
	</configuration>

#### Register description
The *register* node allows to describe one register. It has two attributes:

- id provides the name of this register.
- default provides the default value of this register.  This is a hexadecimal 32-bit value, e.g. starting with “0x”. This value will be displayed when **registerView** will be launched. 

The configuration node contains as many register nodes as there are registers described.

Example:

	<?xml version="1.0"?>
	<configuration title="Chartreuse71 Registers">
		<register id="CORE_CONFIGURATION" default="0xF390149C">
		…
		</register>
		…
	</configuration>

#### Field description
The *field* node allows to describe each bit-field of the register. It has four attributes:

- name provides the name of the bit-field. The name “RESERVED” is a reserved key-word; it indicates that this field is not associated with a particular function and should be zero.
- label provides a short description of the function configured by this bit-field.
- width provides the width of the bit-field (i.e. the number of bits of the bit-field).
- bit provides starting bit position number (from 0 to 31).

The *register* node contains as many field nodes as there are bit-field in the register.
The value node allows to define all allowed values for a bit-field. These values are not necessarily consecutive. It has two attributes:

- val provides an allowed value for this bit-field.
- label provides a short description of the value.

The *field* node contains as many value nodes as there are  allowed values for this bit-field. A field node can have no value node; in this case, the value assigned to this field is taken as such.

Example:

	<?xml version="1.0"?>
	<configuration title="Chartreuse71 Registers">
		<register id="CORE_CONFIGURATION" default="0xF390149C">
			…
			<field name="ITC" label="Interrupt Controller and interrupts"   width="2" bit="12">
				<value val="0" label="No interrupt controller"/>
				<value val="1" label="7 maskable interrupts + NMI"/>
				<value val="2" label="15 maskable interrupts + NMI"/>
				<value val="3" label="31 maskable interrupts + NMI"/>
			</field>
		…
		</register>
		…
	</configuration>

###Registers Viewer User interface
The screen shot below gives an overview of the user interface offered by  **registerView**.

| Legend ||
|------------|-------------------------------------|
| 1.          |One tab per register node defined in the XML configuration file. |
| 2.         | Register area: Display the current value of the register. The value field can be updated by user and after clicking on the "DECODE" button  or by generating a "return", the bit-field display is refreshed with this new value. |
| 3.        | Fields area: buttons to expand or collapse to all  levels of data detail. |
| 4.        | Bit-field display: display first the register and its hexadecimal value. Once expanded, bit-fields and their binary value are displayed. When a bit-field is expanded, all potential values and their description are displayed;  the current value is displayed in bold. By double-clicking on a value, the user changes the current bit-field value and then the register value. |


![registerView User Interface](README_images/registerView01.png  "registerView User Interface")

### USING REGISTERS VIEWER
#### Running Registers Viewer
Once installed (refer to sections *"How To Install And Use This Application*"), the Registers Viewer can be launched in different ways, in command line  mode or through a file explorer if the host system permits. Whatever the method of launch, we recommend activating a script file rather than directly launch the Java program (refer to section *"Launching Script Files*"). In the following examples, regview is the script file delivered in the installation kit for Linux:

	# regViewer is installed in a directory referenced by the PATH environment variable.
	$ regview				
	
	#regViewer is installed in the current directory.
	$ ./regview
	
	# regViewer is installed in the /home/user/bin directory and it is launched using its absolute path
	$ /home/user/bin/regview	.
	
	# regViewerr is installed in the current directory. and is directly launched using Java.
	$ java -cp ./swt-461.jar -cp ./commons-cli-1.3.1.jar -cp ./commons-lang3-3.3.2.jar -jar ./registerView.jar --xml ./desc.xml

#### Command-Line Arguments
It is possible to pass some values from the command line to the Registers Viewer when it is executed. These options are listed in the table below:

| Options | Value and meaning |
|-----------|---------------------------|
|--help, -h | Print this message and exit |
|--version, -V | Print the version information and exit |
|--xml *file*, -x *file* | Register  XML description to be used |
|--debug, -d | Debug mode |

### LAUNCHING SCRIPT FILES
To well-manage the launch of the user's application built over the Registers Viewer, we recommend to adapt the script files provided within the package: *registerView_bin/lin/regview* for Linux platforms and *registerView_bin/win/regview.bat* for Windows platforms.
These scripts:

- specify the parameter of the application: base name of the associated property file, debug mode or not, etc.
- determine the location of the script, assuming that all components needed to launch the application are located in the same directory.
- check the consistency of the environment and the existence of referenced referenced files.
- launch Java with the Registers Viewer.

### STRUCTURE OF THE APPLICATION
This section walks you through **registerView**'s structure. Once you understand this structure, you will easily find your way around in **registerView**'s code base.

``` Bash
$ yaTree
./                                         # Application level
├── README_images/                         # Images for documentation
│   └── registerView01.png                 # 
├── registerView_bin/                      # Binary directory: jars (third-parties and local) and driver
│   ├── Makefile                           # -- Makefile
│   ├── chartreuse71.xml                   # -- XML register definition
│   ├── commons-cli-1.4.jar                # -- Third-party COMMONS CLI jar file
│   ├── commons-lang3-3.9.jar              # -- Third-party COMMONS LANG3 jar file
│   └── swt-linux-412.jar                  # -- Third-party SWT jar file
├── registerView_c/                        # C Source directory
│   ├── Makefile                           # -- Makefile
│   ├── registerView.c                     # -- C main source file (application driver)
│   └── registerView.ggo                   # -- 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── registerView_java/                     # JAVA Source directory
│   ├── registerView/                      # -- IntelliJ registerView project structure
│   │   ├── src/                           # 
│   │   │   ├── Utilities.java             # 
│   │   │   ├── View.java                  # 
│   │   │   ├── registerView.java          # 
│   │   │   └── registerViewBuildInfo.java # 
│   │   └── registerView.iml               # 
│   └── Makefile                           # -- Makefile
├── COPYING.md                             # GNU General Public License markdown file
├── LICENSE.md                             # License markdown file
├── Makefile                               # Top-level Makefile
├── README.md                              # ReadMe Mark-Down file
├── RELEASENOTES.md                        # Release Notes markdown file
└── VERSION                                # Version identification text file

6 directories, 21 files
$
```

## HOW TO BUILD THIS APPLICATION
``` Bash
	$ cd registerView
	$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
``` Bash
	$ cd registerView
	$ make install release
		# Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
	$ registerView -x <file>
			# Assuming that the installation directory is defined
			# in your PATH env variable.
```

## HOW TO PLAY WITH JAVA SOURCES
To play with, we recommend to use **IntelliJ IDEA**, on a Linux platform:

- Launch IntelliJ IDEA
- Click on **File -> Open...** and select *registerView/registerView_java/registerView* project
- It's your turn...

## SOFTWARE REQUIREMENTS
- For usage:
	- JAVA 1.8.0 for usage and development
- For development:
  - IntelliJ IDEA 2019.2.4 (Community Edition) Build #IC-192.7142.36, built on October 29, 2019
  - Openjdk:
    - version "1.8.0_232-ea"
    - OpenJDK Runtime Environment (build 1.8.0_232-ea-8u232-b09-0ubuntu1-b09)
    - OpenJDK 64-Bit Server VM (build 25.232-b09, mixed mode)
  - GNU gengetopt 2.22.6 
Application developed and tested with UBUNTU 16.10 / UBUNTU 17.04 / XUBUNTU 17.04.

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES) .

***