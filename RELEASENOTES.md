# RELEASE NOTES: *registerView*, a registers Viewer

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 2.4.6**:
  - Updated build system components.

- **Version 2.4.5**:
  - Updated build system.

- **Version 2.4.4**:
  - Removed unused files.

- **Version 2.4.3**:
  - Updated build system component(s)

- **Version 2.4.2**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 2.4.1**:
  - Updated SWT library from 4.12 to 4.15.

- **Version 2.4.0**:
  - Removed build method for Windows and associated files.

- **Version 2.3.3**:
  - Some minor changes in project structure and build.

- **Version 2.3.2**:
  - Fixed ./registerView_bin/Makefile (*astyle* target)
  - Fixed ./registerView_c/Makefile (*install* target)

- **Version 2.3.1 - Build 36**:
  - Fixed ./registerView_bin/Makefile (*clean* target)
  - Updated README file.

- **Version 2.3.0 - Build 36**:
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Replaced Release Nores file (this file) by markdown version.
  - Removed *index.html* file (was used for *GitHub*).
  - Moved from GPL v2 to GPL v3.
  - Moved to SWT 4.12. Initially was 4.6.3.
  - Moved to COMMON LANG3 3.9. Initially was 3.5.
  - Improved ./Makefile, ./registerView_bin/Makefile, ./registerView_c/Makefile, ./registerView_java/Makefile.
  - Removed C compilation warnings.

- **Version   2.2.2 - Build 35**:
  - Added ".comment" file in each directory for 'yaTree' utility.

- **Version   2.2.1 - Build 35**:
  - Fixed typos and omission in README file.

- **Version   2.2.0 - Build 35**:
  - Driver is now a C executable. Was a script (bash / cmd) file.
  - Introduced cross generation for Windows (make wall|wclean|winstall|wcleaninstall).
  - Specified foreground and background colors after migration from Ubuntu/Mate
    to XUbuntu/Xfce4 (slightly differents with both 2 window managers).
  - Move to SWT 4.6.3. Previously was 4.6.2.
  - Move to Commons CLI 1.4 Previously was 1.3.1.
  - Move to Commons LANG3 3.5. Previously was 3.3.2

- **Version   2.1.1 - Build 30**:
 - FIX: Added host-independent jar files copy in Windows installation script.

- **Version   2.1.0 - Build 30**:
  - Moved from ECLIPSE Neon.1 Release (4.6.1)  to IntelliJ IDEA 2016.3.2.
